'use strict';

var gulp = require('gulp');
var ngTemplates = require('gulp-ng-templates');
var htmlmin = require('gulp-htmlmin');
 
gulp.task('templates', function () {
  return gulp.src('src/app/**/*.tpl.html')
    .pipe(htmlmin({collapseWhitespace: true}))
    .pipe(ngTemplates({
      filename: 'templates.js',
      module: 'jaliltest.templates',
      path: function (path, base) {
        return path.replace(base, '').replace('/templates', '');
      }
    }))
  .pipe(gulp.dest('src/app'));
});