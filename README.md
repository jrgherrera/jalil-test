# README #

###Instructions###

* Using RWD (Responsive Web Design) techniques and the compass breakpoint mixin, show the sections vertically (one above the other) if the display is a handheld or tablet device, but side-by-side with equal widths for desktop user agents.
* Using SASS write a cross browser gradient mixin and use it to apply different gradients to each section.
* Use [this image](http://dsx.weather.com/util/image/w/68a62f4e-122e-4c72-91b2-ec9f5024e031.jpg?api=7db9fe61-7414-47b5-9871-e17d87b8b6a0&h=598&w=640&v=at) as the image if the device viewing the page is a Retina display.
* Set this page up as an Angular app. Manually initialize the app before the closing body tag.
* Create an angular directive with an interactive element that makes a simple change to the the presentation. For example, click a button and change the background color. Load the directive template from $templateCache. Feel free to be creative or even silly.
* *Bonus question: Create a factory and use it in your directive to calculate the area of the directive element. Display the result of the calculation.

### To run the project ###

* Clone this repo
* Run npm install and bower install
* Run gulp serve (other tasks may not work because I added some dependencies like ng-templates and only updated the serve gulp task)