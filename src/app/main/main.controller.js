(function() {
  'use strict';

  angular
    .module('jaliltest')
    .controller('MainController', MainController);

  /** @ngInject */
  function MainController() {
    var vm = this;

    vm.color = "#ffffff";
  }
})();
