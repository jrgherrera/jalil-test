(function() {
  'use strict';

  angular
    .module('jaliltest')
    .run(runBlock);

  /** @ngInject */
  function runBlock($log) {

    $log.debug('runBlock end');
  }

})();
