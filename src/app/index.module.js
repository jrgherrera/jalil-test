(function() {
  'use strict';

  angular
    .module('jaliltest', ['ngAnimate', 'ngTouch', 'ui.router', 'jaliltest.templates']);

})();
