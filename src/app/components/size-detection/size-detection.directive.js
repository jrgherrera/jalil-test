/* global document:false */
(function() {
  'use strict';

  angular
    .module('jaliltest')
    .directive('sizeDetect', sizeDetect);

  /** @ngInject */
  function sizeDetect(SizeDetect) {
    var directive = {
      restrict: 'A',
      link: function(scope, el, attr) {
        var size = {},
            appendix = document.createElement('span');
        appendix.className = 'appendix';
        if(attr.sizeDetect === "image") {
          el[0].onload = function () {
            size = SizeDetect.getSize(el);
            el.parent().addClass('parent');
            appendix.innerHTML = 'Width: ' + size.width + ' Height: ' + size.height;
            el.after(appendix);
          };
        }
        else {
          size = SizeDetect.getSize(el);
          el.addClass('parent');
          appendix.innerHTML = 'Width: ' + size.width + ' Height: ' + size.height;
          el.append(appendix);
        }
      }
    };

    return directive;
  }

})();
