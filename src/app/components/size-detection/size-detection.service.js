(function() {
  'use strict';

  angular
      .module('jaliltest')
      .factory('SizeDetect', SizeDetect);

  /** @ngInject */
  function SizeDetect() {
    var sd = {};

    sd.getSize = getElementSize;

    return sd;

    function getElementSize (el) {
      return {
        width: el[0].width ? el[0].width : el[0].clientWidth,
        height: el[0].height ? el[0].height : el[0].clientHeight
      };
    }
  }

})();
