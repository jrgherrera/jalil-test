(function() {
  'use strict';

  angular
      .module('jaliltest')
      .factory('RandomColor', RandomColor);

  /** @ngInject */
  function RandomColor() {
    var obj = {};

    obj.getColor = randomColors;

    return obj;

    function randomColors () {
      return '#'+('00000'+(Math.random()*(1<<24)|0).toString(16)).slice(-6);
    }
  }

})();
