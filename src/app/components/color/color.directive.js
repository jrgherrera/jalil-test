(function() {
  'use strict';

  angular
    .module('jaliltest')
    .directive('color', color);

  /** @ngInject */
  function color(RandomColor) {
    var directive = {
      restrict: 'E',
      templateUrl: 'app/components/color/color.tpl.html',
      scope: {
        bodyColor: '='
      },
      link: function($scope, $element) {
        $element.on('click', function (e) {
          e.preventDefault();
          $scope.bodyColor = RandomColor.getColor();
          $scope.$apply();
        });
      }
    };

    return directive;
  }

})();
