(function() {
  'use strict';

  angular
    .module('jaliltest')
    .directive('mobileDetect', mobileDetect);

  /** @ngInject */
  function mobileDetect(DetectService) {
    var directive = {
      restrict: 'A',
      link: function(scope, el) {
        if(DetectService.md.mobile() !== null) {
          scope.isMobile = true;
          el.addClass('is-mobile');
        }
        else {
          scope.isMobile = false;
          el.addClass('is-desktop');
        }
      }
    };

    return directive;
  }

})();
