/* global MobileDetect:false */
(function() {
  'use strict';

  angular
      .module('jaliltest')
      .service('DetectService', DetectService);

  /** @ngInject */
  function DetectService($window) {

    var md = new MobileDetect($window.navigator.userAgent);

    this.md = getMd();

    function getMd() {
      return md;
    }
  }

})();
